import sys
import requests
import json

import cognitive_face as CF
from PIL import Image, ImageDraw, ImageFont
import pickle
from sys import stdout

subscription_key = None

SUBSCRIPTION_KEY = '0a773ee8fbd64e57a6bf3336929b8264'
BASE_URL = 'https://proyectofaces2.cognitiveservices.azure.com/face/v1.0/'
CF.BaseUrl.set(BASE_URL)
CF.Key.set(SUBSCRIPTION_KEY)


class Persona:
    """[Create the class that represents a person]
    """
    def __init__(self, identificacion, personId, nombre, edad, genero, urlfoto):
        """[It is the constructor of the class]
        """
        self.identificacion = identificacion
        self.personId = personId
        self.nombre = nombre
        self.edad = edad 
        self.genero = genero
        self.urlfoto = urlfoto
    
    
    def __str__(self):
        return "Identificacion: {}, PersonId: {}, Nombre: {}, Edad: {}, Genero: {}, Urlfoto: {}".format(self.identificacion, self.personId, 
        self.nombre, self.edad, self.genero, self.urlfoto)



class Familia(Persona):
    """[create a class that represents a relative]

    Args:
        Persona ([class]): [parent class]
    """
    def __init__(self, identificación, personId, nombre, edad, genero,  urlfoto, tipo_familia, miembros_familia):
        """[It is the constructor of the class]
        """
        Persona.__init__(self, identificación, personId, nombre, edad, genero,  urlfoto)
        self.tipo_familia = tipo_familia
        self.miembros_familia = miembros_familia
        
    def __str__(self):
        return "Identificacion: {}, PersonId: {}, Nombre: {}, Edad: {}, Genero: {}, Urlfoto: {}, Tipo de Familia: {}, Miembros de su familia: {}".format(self.identificacion, 
        self.personId, self.nombre, self.edad, self.genero, self.urlfoto, self.tipo_familia, self.miembros_familia)



class Amigos(Persona):
    """[create a class that represent a friend]

    Args:
        Persona ([class]): [parent class]
    """
    def __init__(self, identificación, personId, nombre, edad, genero,  urlfoto, años_amista,gustos):
        """[It is the constructor of the class]
        """
        Persona.__init__(self, identificación, personId, nombre, edad, genero,  urlfoto)
        self.años_amista = años_amista
        self.gustos = gustos

    def __str__(self):
        return "Identificacion: {}, PersonId: {}, Nombre: {}, Edad: {}, Genero: {}, Urlfoto: {}, Años de amista: {}, Gustos Musicales: {}".format(self.identificacion, self.personId, 
        self.nombre, self.edad, self.genero, self.urlfoto, self.años_amista, self.gustos)



class Famoso(Persona):
    """[create a class that represents a famous person]

    Args:
        Persona ([class]): [parent class]
    """
    def __init__(self, identificación, personId, nombre, edad, genero,  urlfoto, nacionalidad, estado):
        """[It is the constructor of the class]
        """
        Persona.__init__(self, identificación, personId, nombre, edad, genero,  urlfoto)
        self.nacionalidad = nacionalidad
        self.estado = estado
        
    def __str__(self):
        return "Identificacion: {}, PersonId: {}, Nombre: {}, Edad: {}, Genero: {}, Urlfoto: {}, Nacionalidad: {}, Estado Civil: {}".format(self.identificacion, self.personId, 
        self.nombre, self.edad, self.genero, self.urlfoto, self.nacionalidad, self.estado)


def emotions(picture):
    """[This function owes the image to Microsoft Azure and downloads the data in dictionary form]

    Args:
        picture ([str]): [image path ]
    """
    #headers = {'Ocp-Apim-Subscription-Key': 'e70e11c9cb684f21b8b37313fd60e5bc'}
    image_path = picture
    #https://docs.microsoft.com/en-us/azure/cognitive-services/computer-vision/quickstarts/python-disk
    # Read the image into a byte array
    image_data = open(image_path, "rb").read()
    headers = {'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
    'Content-Type': 'application/octet-stream'}
    params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
    }
    response = requests.post(
                             BASE_URL + "detect/", headers=headers, params=params, data=image_data)
    analysis = response.json()
    dic = analysis

    global b
    b = dic

    for e in dic:
        """[This algorithm will extract the data from the dictionary]
        """
        face = e['faceAttributes']

        genero = face['gender']
        edad = face['age']
    
        global  ed
        ed = edad

        global ge 
        ge = genero


def create_person(name, profession, picture, group_id, Identificacion, tipo_familia, miembros_familia, años_amista, gustos,nacionalidad, estado, a):
    """[This function is intended to create a person in a group in Microsoft Azure and store certain data]

    Args:
        name ([int]): [Name of individual]
        profession ([int]): [profession of the person]
        picture ([int]): [Image path]
        group_id ([int]): [Group id]
        Identificacion ([int]): [description]
        tipo_familia ([int]): [What family is it]
        miembros_familia ([int]): [how many members does the family have]
        gustos ([int]): [Musical tastes of the person]
        nacionalidad ([int]): [Nationality of the individual]
        estado ([int]): [Marital status of the individual]
        a ([int]): [Numerical value]
    """
     
    response = CF.person.create(group_id, name, profession)
    
    person_id = response['personId']


    CF.person.add_face(picture, group_id, person_id)
    #print CF.person.lists(PERSON_GROUP_ID)
    
    #Re-entrenar el modelo porque se le agrego una foto a una persona
    CF.person_group.train(group_id)
    #Obtener el status del grupo
    response = CF.person_group.get_status(group_id)
    status = response['status']
    print(status)
    urlfoto = picture
    genero = ge
    edad = ed
    personId = person_id
    nombre = name

    if a == 1:
        """[This algorithm writes to a binary file if the option is 1]
        """
        with open ("personas.bin","ab") as f:
            familia = Familia(Identificacion, personId, nombre, edad, genero, urlfoto, tipo_familia, miembros_familia)
            
            pickle.dump(familia, f, pickle.HIGHEST_PROTOCOL)
    
    if a == 2:
        """[This algorithm writes to a binary file if the option is 2]
        """
        with open ("personas.bin","ab") as f:
            amigos = Amigos(Identificacion, personId, nombre, edad, genero, urlfoto, años_amista, gustos)
            
            pickle.dump(amigos, f, pickle.HIGHEST_PROTOCOL)
    
    if a == 3:
        """[This algorithm writes to a binary file if the option is 3]
        """
        with open ("personas.bin","ab") as f:
            famoso = Famoso(Identificacion, personId, nombre, edad, genero, urlfoto, nacionalidad, estado)
            
            pickle.dump(famoso, f, pickle.HIGHEST_PROTOCOL)

    


def read_file():
    """[This function reads the binary file and prints the data that is stored in it.]
    """
    f= open ("personas.bin","rb")
    f.seek(0)
    flag=0
    while flag == 0:
        try:
            e = pickle.load(f)
            print("Nombre:",e.nombre,",", "Identificacion:", e.identificacion,",", "Años:", e.edad,",", "Path de la foto:", e.urlfoto)
            
        except:
            print("Fin")
            flag = 1
            
    f.close


def abrir_imagen(picture, b):
    '''
    function to open images with frames on faces 
    that receives as parameters the image and the face rectangles of each face
    '''
    imagen = Image.open(picture)
    for ro in b:
        face = ro['faceRectangle']
        top = face['top'] 
        left = face['left']  
        width = face['width'] 
        height = face['height']
        draw = ImageDraw.Draw(imagen)
        draw.rectangle((left, top, left+width, top+ height), outline= "Blue", width = 5)
        font = ImageFont.truetype("C:/Users/Dell/Downloads/Carpeta de el proyecto/public_semana-9_Arial_Unicode.ttf",35)
        draw.text((left,top-40), "Persona", font=font, fill="black")
        
    imagen.show(imagen)


def read_file1():
    """[This function reads the binary file and prints the data that is stored in it.]
    """
    f= open ("personas.bin","rb")
    f.seek(0)
    flag=0
    while flag == 0:
        try:
            e = pickle.load(f)
            nom=e.nombre 
            eda=e.edad
            path = e.urlfoto
            gen = e.gen
            

            if path == picture:
                print("Nombre: {}, Edad: {}, Genero: {}".format(nom, eda, gen))
            
        except:
            print("Fin")
            flag = 1
            
    f.close
    

#Imprimir la lista de personas que pertenecen a un grupo
#group_id es el id del grupo que se desea imprimir sus personas
def print_people(group_id):
    """[This feature will find all people in a Microsoft Azure group]

    Args:
        group_id ([int]): [id of the group to search]

    Returns:
        [str]: [returns all people stored in that group]
    """
    #Imprimir la lista de personas del grupo
    data = CF.person.lists(group_id)
    l1=[]
    l2=[]
    def read_file2():
        """[This function reads the data from the binary file and puts it in a list]
        """
        f= open ("personas.bin","rb")
        f.seek(0)
        flag=0
        while flag == 0:
            try:
                e = pickle.load(f)
                nom=e.nombre 
                eda=e.edad
                gen = e.genero
                fsa = e.personId
                    
                lista=[]
                if fsa == Id:
                    lista.append(eda)
                    lista.append(nom)
                    lista.append(gen)
                    l1.append(lista)
                    l2.append(lista)
            except:
                flag = 1              
        f.close

    for x in data:
        Id = x['personId']
        read_file2()    
    def mayor_lista(l):
        """[This function finds the largest number in the lists]

        Args:
            l (list): [messy list]

        Returns:
            [list]: [accommodated list]
        """
        if len(l)==0:
            return (None)
                
        else:
            mayor=l[0]
                    
            for x in l[1:]:
            
                if x>mayor:
                    mayor=x
                
            return(mayor) 
        
    def ordenamiento_insercion(l):
        """[This function sorts from highest to lowest with insert method]

        Args:
            l ([list]): [messy list]

        Returns:
            [list]: [ordered list]
        """
        resultado=[]
        while len(l)>0:
            mayor=mayor_lista(l)
            resultado.insert(0,mayor)
            l.remove(mayor)
        return resultado
    s=ordenamiento_insercion(l1)
    
    for el in s:
        a1=el[1]
        a2=el[0]
        a3=el[2]
        print("Nombre:",a1, "Edad:",a2, "Genero: ",a3)
    print("========================================")
    def menor_lista(l1):
        """[This function finds the smallest number in the lists]

        Args:
            l (list): [messy list]

        Returns:
            [list]: [accommodated list]
        """
        if len(l1)==0:
            return (None)
                
        else:
            menor=l1[0]
                    
            for t in l1[1:]:
            
                if t<menor:
                    menor=t
                
            return(menor) 
        
    def ordenamiento_insercion1(l1):
        """[This function sorts from least to greatest with the insert method]

        Args:
            l1 ([list]): [messy list]

        Returns:
            [list]: [ordered list]
        """
        resultado1=[]
        while len(l1)>0:
            menor=menor_lista(l1)
            resultado1.insert(0,menor)
            l1.remove(menor)
        return resultado1
    d = ordenamiento_insercion1(l2)
    for e in d:
        a3=e[1]
        a4=e[0]
        a5=e[2]
        print("Nombre:",a3, "Edad:",a4, "Genero: ",a5)
    print("fin")
    print("========================================")

if __name__ == "__main__":
    """[This algorithm shows a series of options to choose from]
    """
    print("Dígite 1 -> Crear persona en un grupo")
    print("Digite 2 -> Ver registros")
    print("Digi 3 -> Abrir imagen agregada y consultar datos")
    print("Digite 4 -> Ver historial de personas por grupo")
    print("==================================")
    case = int(input())

    if case > 4 or case == 0:
        print ("Esa opcion no esta disponible:\n","Opciones 1-3")

    if case == 1 :             #Crear crear una persona
        print("==================================")
        print("Grupos Disponibles")
        print("1. Familia")
        print("2. Amigos")
        print("3. Famosos")
        print("==================================")
        a=int(input())
        if a == 1:
            """[This algorithm asks for certain data and saves it in classes and vinario file]
            """
            name = input("Dígite el nombre: ")
            profession = input("Dígite la profesión: ")
            picture = input("Dígite el path de la imagen: ")
            group_id = 1
            Identificacion = input("Digite su Identificacion: ")
            tipo_familia = input("Que tipo de familia es: ")
            miembros_familia = input("Cuantos mienbros conforman la familia: ")
            años_amista = ''
            gustos = ''
            nacionalidad = '' 
            estado = ''
            emotions(picture)
            create_person(name, profession, picture, group_id, Identificacion, tipo_familia, miembros_familia, años_amista, gustos, nacionalidad, estado, a)
            

        if a == 2:
            """[This algorithm asks for certain data and saves it in classes and vinario file]
            """
            name = input("Dígite el nombre: ")
            profession = input("Dígite la profesión: ")
            picture = input("Dígite el path de la imagen: ")
            group_id = 2
            Identificacion = input("Digite su Identificacion: ")
            años_amista = input("Años de amistad: ")
            gustos = input("Gustos musicales: ")
            tipo_familia = años_amista
            miembros_familia = gustos
            nacionalidad = ''
            estado = ''
            emotions(picture)
            create_person(name, profession, picture, group_id, Identificacion,tipo_familia, miembros_familia, años_amista, gustos,nacionalidad, estado,a)
            

        if a == 3:
            """[This algorithm asks for certain data and saves it in classes and vinario file]
            """
            name = input("Dígite el nombre: ")
            profession = input("Dígite la profesión: ")
            picture = input("Dígite el path de la imagen: ")
            group_id = 3
            Identificacion = input("Digite su Identificacion: ")
            nacionalidad = input("Nacionalidad: ")
            estado = input("Estado civil: ")
            tipo_familia = nacionalidad
            miembros_familia = estado
            años_amista = nacionalidad
            gustos = estado
            emotions(picture)
            create_person(name, profession, picture, group_id, Identificacion, tipo_familia, miembros_familia, años_amista, gustos, nacionalidad, estado, a)
            

    elif case == 2 :
        """[This algorithm calls the read_file function to read the data from the vinario file]
        """
        print("===============================")
        print("Registro:")
        read_file()
        

    elif case == 3 :
        """[This algorithm calls the open image function and shows the person with a box on the face and the stored data]
        """
        print("Digite Path de la imagen: ")
        picture = input()
        emotions(picture)
        abrir_imagen(picture, b)
        read_file1()

    elif case == 4 :
        """[This algorithm calls the function print people to show all available people in a group]
        """
        print("===========================================")
        print("Digite 1 -> Familia")
        print("Digite 2 -> Amigos")
        print("Digite 3 -> Famosos")
        print("===========================================")
        group_id = int(input("Dígite el id del grupo: "))
        print("===========================================")
        print_people(group_id)
        
        


#Amigos
# C:\Users\Dell\Desktop\II Semestre\Taller\PT2\Profesor.jpg
# C:\Users\Dell\Desktop\II Semestre\Taller\PT2\p1.jpg

#Famosos
# C:\Users\Dell\Desktop\II Semestre\Taller\PT2\Brenda-Muñoz.jpeg
# C:\Users\Dell\Desktop\II Semestre\Taller\PT2\prueba.jpg
# C:\Users\Dell\Desktop\II Semestre\Taller\PT2\ro.jpg

#Familia
# C:\Users\Dell\Desktop\II Semestre\Taller\PT2\amigo.jpg
# C:\Users\Dell\Desktop\II Semestre\Taller\PT2\p4.jpg